import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { WsDetailPage } from './ws-detail.page';

const routes: Routes = [
  { path: '', component: WsDetailPage}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class WsDetailPageRoutingModule {}
