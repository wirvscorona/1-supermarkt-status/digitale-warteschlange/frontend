import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';
import { WsDetailPageRoutingModule } from './ws-detail-routing.module';
import { WsDetailPage } from './ws-detail.page';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    WsDetailPageRoutingModule
  ],
  declarations: [WsDetailPage]
})
export class WsDetailPageModule {}
