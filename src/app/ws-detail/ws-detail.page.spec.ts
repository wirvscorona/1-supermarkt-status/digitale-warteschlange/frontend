import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { WsDetailPage } from './ws-detail.page';

describe('WsDetailPage', () => {
  let component: WsDetailPage;
  let fixture: ComponentFixture<WsDetailPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WsDetailPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(WsDetailPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
