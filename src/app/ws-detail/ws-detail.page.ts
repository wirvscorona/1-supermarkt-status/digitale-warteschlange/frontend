import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';

import { Map, latLng, tileLayer, Layer, marker } from 'leaflet';
import { QUEUES } from '../mock-queues';
import { Queue } from '../queue';

@Component({
  selector: 'app-ws-detail',
  templateUrl: './ws-detail.page.html',
  styleUrls: ['./ws-detail.page.scss'],
})
export class WsDetailPage implements OnInit {

  queue: Queue;
  activeView = "list";

  map: Map;

  ionViewDidEnter() { this.leafletMap(); }

  leafletMap() {
    // In setView add latLng and zoom
    this.map = new Map('mapId').setView([this.queue.address.lat, this.queue.address.long], 100);
    tileLayer('http://server.arcgisonline.com/ArcGIS/rest/services/World_Street_Map/MapServer/tile/{z}/{y}/{x}', {
      attribution: 'edupala.com © ionic LeafLet',
    }).addTo(this.map);
    marker([this.queue.address.lat, this.queue.address.long]).addTo(this.map)
      .bindPopup(this.queue.name, this.queue.category)
  }

  /** Remove map when we have multiple map object */
  ionViewWillLeave() {
    this.map.remove();
  }

  ngOnInit() {
    let id : number = Number.parseInt(this.route.snapshot.paramMap.get('id'));

    this.queue = QUEUES[id];
  }

  constructor(private route: ActivatedRoute) {}
}
