import { Address } from './address'

export interface Queue {
    id: number;
    name: string;
    email: string;
    description: string;
    address: Address;
    capacity: number;
    category: string;
    avg_wait_time: number;
    act_wait_time: number;
    status: string;
}
