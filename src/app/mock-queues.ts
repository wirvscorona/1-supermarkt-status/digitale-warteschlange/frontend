import { Queue } from "./queue";

export const QUEUES: Queue[] = [
    {
        id: 0,
        name: "Edeka Görge Wiesenstraße",
        email: "test@example.com",
        description: "Klopapier ist ausverkauft",
        address: {
            address: "Wiesenstraße 9",
            plz: 38102,
            city: "Braunschweig",
            lat:
            52.26862,
            long: 10.53861
        },
        capacity: 20,
        category: "Einzelhandel für Lebensmittel",
        avg_wait_time: 30,
        act_wait_time: 10,
        status: "0.6"
    },
    {
        id: 1,
        name: "Edeka Center Braunschweig",
        email: "test@example.com",
        description: "Eingangskontrolle vor der Gemüseabteilung",
        address: {
            address: "BraWo-Allee 1",
            plz: 38102,
            city: "Braunschweig",
            lat: 52.255489,
            long: 10.54299
        },
        capacity: 300,
        category: "Einzelhandel für Lebensmittel",
        avg_wait_time: 10,
        act_wait_time: 15,
        status: "0.7"
    },
    {
        id: 2,
        name: "Rewe Hamburger Straße",
        email: "test@example.com",
        description: "Nudeln sind ausverkauft",
        address: {
            address: "Wendenring 1",
            plz: 38114,
            city: "Braunschweig",
            lat: 52.274921,
            long: 10.52108
        },
        capacity: 100,
        category: "Einzelhandel für Lebensmittel",
        avg_wait_time: 40,
        act_wait_time: 50,
        status: "0.9"
    },
    {
        id: 3,
        name: "Gauß-Apotheke",
        email: "test@example.com",
        description: "Wir haben jetzt auch sonntags für Sie geöffnet",
        address: {
            address: "Wendenring 1",
            plz: 38114,
            city: "Braunschweig",
            lat: 52.274921,
            long: 10.52108
        },
        capacity: 10,
        category: "Apotheken",
        avg_wait_time: 30,
        act_wait_time: 50,
        status: "0.9"
    },
    {
        id: 4,
        name: "Aldi Süd",
        email: "test@example.com",
        description: "Bitte den Hintereingang benutzen",
        address: {
            address: "Schwanthalerstraße 14",
            plz: 80336,
            city: "München",
            lat: 48.13755,
            long: 11.56232
        },
        capacity: 10,
        category: "Einzelhandel für Lebensmittel",
        avg_wait_time: 15,
        act_wait_time: 10,
        status: "0.5"
    },
    {
        id: 5,
        name: "BK Tankstelle",
        email: "test@example.com",
        description: "Bieten ganztägig auch warme Snacks an",
        address: {
            address: "Baaderstraße 6",
            plz: 80469,
            city: "München",
            lat: 48.133148,
            long: 11.58084
        },
        capacity: 5,
        category: "Tankstellen",
        avg_wait_time: 10,
        act_wait_time: 60,
        status: "1"
    }
]
