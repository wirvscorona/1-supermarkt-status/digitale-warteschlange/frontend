export interface Address {
    address: string; // steet + number
    plz: number;
    city: string;
    lat: number;
    long: number;
}
