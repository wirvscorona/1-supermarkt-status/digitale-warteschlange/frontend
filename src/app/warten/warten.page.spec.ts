import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { WartenPage } from './warten.page';

describe('WartenPage', () => {
  let component: WartenPage;
  let fixture: ComponentFixture<WartenPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WartenPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(WartenPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
