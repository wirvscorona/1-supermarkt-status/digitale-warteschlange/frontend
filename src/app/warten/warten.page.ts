import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { AlertController } from '@ionic/angular';
import { QUEUES } from '../mock-queues';
import { Queue } from '../queue';


@Component({
  selector: 'app-warten',
  templateUrl: './warten.page.html',
  styleUrls: ['./warten.page.scss'],
})
export class WartenPage implements OnInit {

  queue: Queue;
  activeView = "list";

  constructor(public alertController: AlertController,
  private route: ActivatedRoute) {}

  async presentAlert() {
    const alert = await this.alertController.create({
      header: 'Warteschlange wirklich verlassen?',
      buttons: [
        {
          text: 'Ja',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Ja: blah');
          }
        }, {
          text: 'Nein',
          handler: () => {
            console.log('Confirm Okay');
          }
        }
      ]
    });
    await alert.present();
  }
  ngOnInit() {
    let id : number = Number.parseInt(this.route.snapshot.paramMap.get('id'));

    this.queue = QUEUES[id];
  }
}
