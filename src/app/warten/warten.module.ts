import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { WartenPageRoutingModule } from './warten-routing.module';

import { WartenPage } from './warten.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    WartenPageRoutingModule
  ],
  declarations: [WartenPage]
})
export class WartenPageModule {}
