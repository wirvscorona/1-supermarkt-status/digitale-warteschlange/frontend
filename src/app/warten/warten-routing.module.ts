import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { WartenPage } from './warten.page';

const routes: Routes = [
  {
    path: '',
    component: WartenPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class WartenPageRoutingModule {}
