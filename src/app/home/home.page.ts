import { Component, OnInit, ViewChild } from '@angular/core';
//import { MapInfoWindow, MapMarker } from '@angular/google-maps';

import { IonSearchbar } from "@ionic/angular";

import { QUEUES } from '../mock-queues';
import { Queue } from '../queue';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {

  /*
  @ViewChild(MapInfoWindow, {static: false}) infoWindow: MapInfoWindow;

  center = {lat: 24, lng: 12};
  markerOptions = {draggable: false};
  markerPositions: google.maps.LatLngLiteral[] = [];
  zoom = 4;
  display?: google.maps.LatLngLiteral;

  addMarker(event: google.maps.MouseEvent) {
    this.markerPositions.push(event.latLng.toJSON());
  }

  move(event: google.maps.MouseEvent) {
    this.display = event.latLng.toJSON();
  }

  openInfoWindow(marker: MapMarker) {
    this.infoWindow.open(marker);
  }

  removeLastMarker() {
    this.markerPositions.pop();
  }

  segmentChanged(ev: any) {
    console.log('Segment changed', ev);
    this.activeView = ev.detail.value;
  }

  */

  constructor() {}

  ngOnInit() {

  }

  activeView = "list";

  public searchTerm: string = "";
  public queues : Array<Queue> = new Array<Queue>();

  @ViewChild('searchBar', {static: true}) searchbar: IonSearchbar;

  focusSearch() {
    this.searchbar.setFocus();
  }

  setFilteredItems() {
    this.queues.length = 0;
    if(this.searchTerm.length == 0)
      return;
    //equestAnimationFrame(() => {
      QUEUES.forEach(item => {
        const shouldShow = item.address.plz.toString().startsWith(this.searchTerm) || item.address.city.toLowerCase().startsWith(this.searchTerm.toLowerCase()) || item.address.address.toLowerCase().startsWith(this.searchTerm.toLowerCase()) || (this.searchTerm.length >= 3 && item.name.toLowerCase().indexOf(this.searchTerm.toLowerCase()) > -1);
        if(shouldShow)
          this.queues.push(item);
      });
    //});
  }

}
